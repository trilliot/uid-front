FROM golang:1.15-alpine AS build

WORKDIR /build
ENV GOPRIVATE="gitlab.com/trilliot/*" \
    GOFLAGS="-mod=readonly"

# Security
RUN apk --no-cache add ca-certificates git && \
    adduser \
        --disabled-password \
        --gecos "" \
        --home "/app" \
        --shell "/sbin/nologin" \
        --no-create-home \
        --uid "10001" \
        "appuser"

# Prepare .netrc for private repositories
ARG GITLAB_USER
ARG GITLAB_PASS
RUN echo "machine gitlab.com login $GITLAB_USER password $GITLAB_PASS" >> ~/.netrc

# Get dependencies
COPY go.mod go.sum ./
RUN go mod download && go mod verify

# Build app
COPY . .
RUN CGO_ENABLED=0 GOOS=linux go build -v -a -installsuffix cgo .

# ---

FROM scratch

COPY --from=build /etc/passwd /etc/group /etc/
COPY --from=build /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/ca-certificates.crt
COPY --from=build /build/uid-front /app/uid-front
COPY ./templates/* /app/templates/

USER appuser:appuser
EXPOSE 8080

ENTRYPOINT [ "/app/uid-front", "-templates-path", "/app/templates" ]
