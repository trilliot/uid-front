package main

import (
	"fmt"
	"html/template"
	"log"
	"net/http"
	"path"

	"github.com/hashicorp/consul/api"
	"github.com/hashicorp/consul/connect"
	"github.com/namsral/flag"
	"gitlab.com/trilliot/uid-api/client"
)

var (
	httpBind      = flag.String("http-bind", "127.0.0.1:8080", "http binding address")
	uidApiAddr    = flag.String("uid-api-addr", "http://127.0.0.1:8080", "uid-api base address")
	templatesPath = flag.String("templates-path", "", "path to templates directory")
	consulConnect = flag.Bool("consul-connect", false, "enable consul connect native mode")
)

func main() {
	flag.Parse()

	service, err := setupConsul()
	if err != nil {
		log.Fatalf("could not setup consul: %v", err)
	}
	defer service.Close()

	client := getClient(service)

	mux := http.NewServeMux()
	mux.Handle("/", showHandler(client))
	log.Fatal(http.ListenAndServe(*httpBind, mux))
}

func showHandler(client *client.Client) http.Handler {
	var template = template.Must(template.ParseFiles(path.Join(*templatesPath, "show.html")))
	type templateData struct {
		XID string
	}

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.Method != http.MethodGet {
			w.WriteHeader(http.StatusMethodNotAllowed)
			return
		}

		xid, err := client.XID()
		if err != nil {
			log.Printf("could not get xid: %v", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		data := templateData{
			XID: xid,
		}

		if err := template.ExecuteTemplate(w, "show.html", data); err != nil {
			log.Printf("could not execute template: %v", err)
		}
	})
}

func getClient(service *connect.Service) *client.Client {
	if !*consulConnect {
		return client.NewClient(*uidApiAddr)
	}
	return client.NewConsulClient(service, *uidApiAddr)
}

func setupConsul() (*connect.Service, error) {
	client, err := api.NewClient(api.DefaultConfig())
	if err != nil {
		return nil, fmt.Errorf("could not create client: %w", err)
	}

	service, err := connect.NewService("uid-front", client)
	if err != nil {
		return nil, fmt.Errorf("could not create service: %w", err)
	}

	return service, nil
}
