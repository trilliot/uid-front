module gitlab.com/trilliot/uid-front

go 1.15

require (
	github.com/hashicorp/consul v1.9.4
	github.com/hashicorp/consul/api v1.8.1
	github.com/namsral/flag v1.7.4-pre
	gitlab.com/trilliot/uid-api v0.0.0-20210402161255-09b172f79080
)
